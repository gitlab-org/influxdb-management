require 'dotenv'
require 'influxdb'

Dotenv.load('.env')

CLIENT = InfluxDB::Client.new(
  ENV.fetch('INFLUX_DATABASE'),
  host: ENV.fetch('INFLUX_HOST'),
  username: ENV.fetch('INFLUX_USER'),
  password: ENV.fetch('INFLUX_PASSWORD'),
  port: ENV.fetch('INFLUX_PORT').to_i
)

QUERIES = File.expand_path('../queries', __FILE__)

desc 'Sets up the retention policies'
task :policies do
  db = ENV.fetch('INFLUX_DATABASE')

  policies = CLIENT.list_retention_policies(db)
  policy_names = policies.map { |h| h['name'] }
  default = policies.find { |h| h['default'] }

  unless default['duration'] == '1h'
    CLIENT.alter_retention_policy(default['name'], db, '1h', 1, true)
  end

  unless policy_names.include?('downsampled')
    CLIENT.create_retention_policy('downsampled', db, '7d', 1)
  end

  unless policy_names.include?('year')
    CLIENT.create_retention_policy('year', db, '52w', 1)
  end

  unless policy_names.include?('forever')
    CLIENT.create_retention_policy('forever', db, 'INF', 1)
  end
end

desc 'Updates the continuous queries in the database'
task :queries do
  db = ENV.fetch('INFLUX_DATABASE')
  policies = CLIENT.list_retention_policies(db)
  default_retention_policy = policies.find { |h| h['default'] }.fetch('name')

  Dir[File.join(QUERIES, '**', '*.txt')].each do |file|
    name = File.basename(file, File.extname(file))
    query = File.read(file).strip

    if query.include?('$DATABASE')
      query.gsub!('$DATABASE', db)
    else
      raise "The query in #{file} must specify the database using `ON $DATABASE`"
    end

    if query.include?('$DEFAULT_RETENTION_POLICY')
      query.gsub!('$DEFAULT_RETENTION_POLICY', default_retention_policy)
    else
      raise "The query in #{file} must specify the default retention policy " \
        "for source measurements using `$DEFAULT_RETENTION_POLICY`"
    end

    puts "Creating continuous query #{name}"

    begin
      CLIENT.query("DROP CONTINUOUS QUERY #{name} ON #{db}")
    rescue InfluxDB::QueryError => error
      raise unless error.message.include?('query not found')
    end

    CLIENT.query(query)
  end
end

task default: [:policies, :queries]
