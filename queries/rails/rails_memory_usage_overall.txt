CREATE CONTINUOUS QUERY rails_memory_usage_overall ON $DATABASE
BEGIN
    SELECT mean(value) AS memory_mean,
        percentile(value, 95) AS memory_95th,
        percentile(value, 99) AS memory_99th,
        max(value) AS memory_max
    INTO downsampled.rails_memory_usage_overall
    FROM "$DEFAULT_RETENTION_POLICY".rails_memory_usage
    GROUP BY time(1m)
END
