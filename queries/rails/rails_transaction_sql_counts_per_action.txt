CREATE CONTINUOUS QUERY rails_transaction_sql_counts_per_action ON $DATABASE
BEGIN
    SELECT max(sql_count) AS sql_count_max,
        mean(sql_count) AS sql_count_mean,
        percentile(sql_count, 95) AS sql_count_95th,
        percentile(sql_count, 99) AS sql_count_99th
    INTO downsampled.rails_transaction_sql_counts_per_action
    FROM "$DEFAULT_RETENTION_POLICY".rails_transactions
    WHERE action =~ /.+/ AND action !~ /^Grape#/
    GROUP BY time(1m), action
END
