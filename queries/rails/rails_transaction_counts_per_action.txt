CREATE CONTINUOUS QUERY rails_transaction_counts_per_action ON $DATABASE
BEGIN
    SELECT count("duration") AS count
    INTO downsampled.rails_transaction_counts_per_action
    FROM "$DEFAULT_RETENTION_POLICY".rails_transactions
    WHERE action =~ /.+/ AND action !~ /^Grape#/
    GROUP BY time(1m), action
END
