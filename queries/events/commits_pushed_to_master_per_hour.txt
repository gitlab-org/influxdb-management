CREATE CONTINUOUS QUERY commits_pushed_to_master_per_hour ON $DATABASE
BEGIN
    SELECT sum(count) AS "count"
    INTO year.commits_pushed_to_master_per_hour
    FROM "$DEFAULT_RETENTION_POLICY".events
    WHERE "event" = 'push_commit'
    AND "branch" = 'master'
    GROUP BY time(1h)
END
