CREATE CONTINUOUS QUERY grape_markdown_timings_overall ON $DATABASE
BEGIN
    SELECT mean("duration") AS duration_mean,
        percentile("duration", 95) AS duration_95th,
        percentile("duration", 99) AS duration_99th
    INTO downsampled.grape_markdown_timings_overall
    FROM "$DEFAULT_RETENTION_POLICY".rails_method_calls
    WHERE (action !~ /.+/ OR action =~ /^Grape#/)
    AND method =~ /^Banzai/
    GROUP BY time(1m)
END
