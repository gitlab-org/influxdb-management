CREATE CONTINUOUS QUERY grape_transaction_new_redis_connection_counts_per_action ON $DATABASE
BEGIN
    SELECT max(new_redis_connections) AS new_redis_connections_max,
        mean(new_redis_connections) AS new_redis_connections_mean,
        percentile(new_redis_connections, 95) AS new_redis_connections_95th,
        percentile(new_redis_connections, 99) AS new_redis_connections_99th
    INTO downsampled.grape_transaction_new_redis_connection_counts_per_action
    FROM "$DEFAULT_RETENTION_POLICY".rails_transactions
    WHERE action !~ /.+/
    OR action =~ /^Grape#/
    GROUP BY time(1m), action
END
