CREATE CONTINUOUS QUERY grape_allocated_memory_per_action ON $DATABASE
BEGIN
    SELECT percentile(allocated_memory, 95) AS allocated_memory_95th,
           percentile(allocated_memory, 99) AS allocated_memory_99th,
           mean(allocated_memory) AS allocated_memory_mean,
           max(allocated_memory) AS allocated_memory_max
    INTO downsampled.grape_allocated_memory_per_action
    FROM "$DEFAULT_RETENTION_POLICY".rails_transactions
    WHERE action =~ /^Grape#/
    GROUP BY time(1m), action
END
