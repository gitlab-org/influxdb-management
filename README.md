# InfluxDB Management

Manually managing an InfluxDB database is a bit annoying. For example, whenever
the list of continuous queries is updated one has to copy-paste the whole list
into an InfluxDB console and run every query.

This repository contains some scripts to more easily keep your database up to
date so it can be used for storing GitLab performance metrics.

## Requirements

* Ruby 2.x
* Bundler

## Installation

First make sure you have Bundler installed:

    gem install bundler --no-ri --no-rdoc

Once Bundler is installed you can install the dependencies of this project:

    bundle install

Next, make sure you have the following details of your InfluxDB database:

* A username
* A password
* The hostname of the server running the database
* The database name (usually "gitlab")

These details are needed to connect to the InfluxDB database. Note that the user
must be able to execute the following commands:

* `CREATE CONTINUOUS QUERY`
* `DROP CONTINUOUS QUERY`
* `CREATE RETENTION POLICY`

Once you have these details you'll need to set up a configuration file. To do
so, run the following command:

    cp .env.example .env

Now edit `.env` using your favourite editor and make sure that the various
variables in this file contain the correct values.

## Usage

Once configured you can set up the retention policies, continuous queries, etc
by running the following command:

    bundle exec rake

## Structure

Each query resides in its own file in a sub directory (per category) in the
`queries/` directory. The names of the files should match the name of the
continuous query, the extension should be `.txt`.

## Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)
